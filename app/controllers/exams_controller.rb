class ExamsController < ApplicationController
  before_action :set_exam, only: [:show, :edit, :update, :destroy]

  # GET /exams
  # GET /exams.json
  def index
    @filter = Exam::Filter.new(params[:exam_filter])
    @exams = Exam.search(@filter).paginate(page: params[:page])
  end

  # GET /exams/1
  # GET /exams/1.json
  def show
  end

  # GET /exams/new
  def new
    @exam = Exam.new
  end

  # GET /exams/1/edit
  def edit
  end

  # POST /exams
  # POST /exams.json
  def create
    @exam = Exam.new(exam_params)

    respond_to do |format|
      if @exam.save
        format.html { redirect_to exams_url, notice: 'Exam was successfully created.' }
        format.json { render :show, status: :created, location: @exam }
      else
        format.html { render :new }
        format.json { render json: @exam.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exams/1
  # PATCH/PUT /exams/1.json
  def update
    respond_to do |format|
      if @exam.update(exam_params)
        format.html { redirect_to exams_url, notice: 'Exam was successfully updated.' }
        format.json { render :show, status: :ok, location: @exam }
      else
        format.html { render :edit }
        format.json { render json: @exam.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exams/1
  # DELETE /exams/1.json
  def destroy
    @exam.destroy
    respond_to do |format|
      format.html { redirect_to exams_url, notice: 'Exam was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def parse_raw_mutations
      raw_mutations = params[:raw_mutations].split(",").map(&:strip)

      raw_mutations.map do |x|
        abbr = x.split("/")

        base = abbr.shift

        if abbr.any?
          abbr.map {|q| base + q}
        else
          base
        end
      end.flatten.map {|s| Mutation.find_or_initialize_by(code: s)}
    end

    def parse_raw_drugs
      raw_drugs = params[:raw_drugs].split(",").map(&:strip)

      raw_drugs.map {|d| Drug.find_or_initialize_by(name: d)}
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_exam
      @exam = Exam.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def exam_params
      mutations = parse_raw_mutations
      drugs     = parse_raw_drugs

      params.require(:exam).permit(:patient_id, :examination_date, :institution_id, :genotype_id, :group_id, :doctor_id).merge(drugs: drugs, mutations: mutations)
    end
end
