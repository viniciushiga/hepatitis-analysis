json.array!(@institutions) do |institution|
  json.extract! institution, :id, :name, :abbreviation
  json.url institution_url(institution, format: :json)
end
