json.array!(@exams) do |exam|
  json.extract! exam, :id, :patient_id, :examination_date, :institution_id, :genotype_id, :group_id, :doctor_id
  json.url exam_url(exam, format: :json)
end
