json.array!(@genotypes) do |genotype|
  json.extract! genotype, :id, :code
  json.url genotype_url(genotype, format: :json)
end
