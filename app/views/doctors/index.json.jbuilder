json.array!(@doctors) do |doctor|
  json.extract! doctor, :id, :name, :crm, :email, :phone
  json.url doctor_url(doctor, format: :json)
end
