json.array!(@mutations) do |mutation|
  json.extract! mutation, :id, :code
  json.url mutation_url(mutation, format: :json)
end
