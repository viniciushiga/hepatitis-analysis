json.array!(@patients) do |patient|
  json.extract! patient, :id, :name, :gender, :birthday, :medical_record, :intitution_id
  json.url patient_url(patient, format: :json)
end
