# == Schema Information
#
# Table name: doctors
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  crm        :string(255)
#  email      :string(255)
#  phone      :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Doctor < ActiveRecord::Base
  validates :name, presence: true
  validates :crm,  presence: true

  def self.search(name, crm)
    query = all
    query = query.where('name LIKE ?', "%#{name}%") if name
    query = query.where('crm LIKE ?', "%#{crm}%")   if crm
    query
  end
end
