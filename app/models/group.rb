# == Schema Information
#
# Table name: groups
#
#  id         :integer          not null, primary key
#  code       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Group < ActiveRecord::Base
  validates :code, presence: true, uniqueness: true

  def self.search(code)
    query = all
    query = query.where('code LIKE ?', "%#{code}%") if code
    query
  end
end
