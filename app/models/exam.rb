# == Schema Information
#
# Table name: exams
#
#  id               :integer          not null, primary key
#  patient_id       :integer          not null
#  examination_date :date             not null
#  institution_id   :integer          not null
#  genotype_id      :integer          not null
#  group_id         :integer          not null
#  doctor_id        :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_exams_on_doctor_id       (doctor_id)
#  index_exams_on_genotype_id     (genotype_id)
#  index_exams_on_group_id        (group_id)
#  index_exams_on_institution_id  (institution_id)
#  index_exams_on_patient_id      (patient_id)
#

class Exam < ActiveRecord::Base
  belongs_to :patient
  belongs_to :institution
  belongs_to :genotype
  belongs_to :group
  belongs_to :doctor

  has_and_belongs_to_many :mutations, autosave: true
  has_and_belongs_to_many :drugs,     autosave: true

  def self.search(filter)
    query = all
    query = query.where(patient_id: filter.patient_id) if filter.patient_id.present?
    query = query.where(doctor_id: filter.doctor_id) if filter.doctor_id.present?
    query = query.where(group_id: filter.group_id) if filter.group_id.present?
    query
  end

  def raw_mutations
    mutations.map(&:code).join(", ")
  end

  def raw_drugs
    drugs.map(&:name).join(", ")
  end

  class Filter
    include ActiveModel::Model

    attr_accessor :patient_id, :doctor_id, :group_id
  end
end
