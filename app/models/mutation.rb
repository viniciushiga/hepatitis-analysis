# == Schema Information
#
# Table name: mutations
#
#  id         :integer          not null, primary key
#  code       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Mutation < ActiveRecord::Base
  validates :code, presence: true

  def self.search(search)
    if search
      where('code LIKE ?', "%#{search}%")
    else
      all
    end
  end
end
