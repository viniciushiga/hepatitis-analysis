# == Schema Information
#
# Table name: institutions
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  abbreviation :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#

class Institution < ActiveRecord::Base
  validates :name, presence: true

  def self.search(name, abbreviation)
    query = all
    query = query.where('name LIKE ?', "%#{name}%") if name
    query = query.where('abbreviation LIKE ?', "%#{abbreviation}%") if abbreviation

    query
  end
end
