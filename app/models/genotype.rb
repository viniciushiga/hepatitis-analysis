# == Schema Information
#
# Table name: genotypes
#
#  id         :integer          not null, primary key
#  code       :string(255)      not null
#  created_at :datetime
#  updated_at :datetime
#

class Genotype < ActiveRecord::Base
  validates :code, presence: true, uniqueness: true

  def self.search(search)
    if search
      where('code LIKE ?', "%#{search}%")
    else
      all
    end
  end
end
