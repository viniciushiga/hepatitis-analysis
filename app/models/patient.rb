# == Schema Information
#
# Table name: patients
#
#  id             :integer          not null, primary key
#  name           :string(255)      not null
#  gender         :string(1)        not null
#  birthday       :date
#  medical_record :string(255)
#  institution_id :integer
#  created_at     :datetime
#  updated_at     :datetime
#
# Indexes
#
#  index_patients_on_institution_id  (institution_id)
#

class Patient < ActiveRecord::Base
  belongs_to :institution

  validates :name,        presence: true
  validates :gender,      inclusion: { in: Gender.all }
  validates :institution, presence: true

  def self.search(filter)
    query = all
    query = query.where("name LIKE ?", "%#{filter.name}%") if filter.name.present?
    query = query.where("medical_record LIKE ?", "%#{filter.medical_record}%") if filter.medical_record.present?
    query
  end

  class Filter
    include ActiveModel::Model

    attr_accessor :name, :medical_record
  end
end
