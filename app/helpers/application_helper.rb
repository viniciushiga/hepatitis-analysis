module ApplicationHelper
  def submit(form)
    form.submit "Submit", class: "btn btn-primary"
  end

  def cancel(path)
    link_to "Cancel", path, class: "btn btn-default"
  end

  def edit_link(url)
    link_to(url, class: "btn btn-success btn-xs") do
      icon("edit", "Edit")
    end
  end

  def destroy_link(url)
    link_to(url, { :method => :delete, :data => { :confirm => t('.confirm', :default => t("helpers.links.confirm", :default => 'Are you sure?')) }, :class => 'btn btn-danger btn-xs'}) do
      icon("trash-o", "Delete")
    end
  end

  def page_header(content)
    content do
      content_tag(:h1) do
        content.html_safe
      end +
      tag(:hr)
    end
  end

  def content(&block)
    content_tag(:div, class: "row") do
      content_tag(:div, class: "col-lg-12") do
        yield.html_safe
      end
    end
  end

  def filter_panel(&block)
    content do
      content_tag(:div, class: "panel panel-default") do
        content_tag(:div, class: "panel-heading") do
          content_tag(:h3, icon("filter", "Filter by").html_safe, class: "panel-title")
        end +
        content_tag(:div, class: "panel-body") do
          yield.html_safe
        end
      end
    end
  end

  def table(collection, &block)
    content do
      if collection.empty?
        content_tag(:div, class: "well text-center") do
          "No records found!".html_safe
        end
      else
        content_tag(:table, class: "table table-striped") do
          yield.html_safe
        end +
        paginate(collection)
      end
    end
  end

  def paginate(collection)
    content_tag(:div, class: "text-center") do
      will_paginate(collection, renderer: BootstrapPagination::Rails)
    end
  end

  def new_link(url)
    link_to(url, class: 'btn btn-primary btn-xs') do
      icon("file-o", "New")
    end
  end

  def format_datetime(datetime)
    l(datetime, format: :short)
  end

  def search_button
    button_tag(type: "submit", class: "btn btn-default", name: nil) do
      icon("search", "Search")
    end
  end

  def clear_button(path)
    link_to(path, class: "btn btn-default") do
      icon("eraser", "Clear")
    end
  end

  def nav_link(text, link, icon_name = nil)
    recognized = Rails.application.routes.recognize_path(link)
    class_name = recognized[:controller] == params[:controller] ? 'active' : ''

    content_tag(:li, class: class_name) do
      link_to(link) do
        icon(icon_name, text)
      end
    end
  end
end
