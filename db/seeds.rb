# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

%w(A1 A2 B1 B2 C1 C2 D1 D2 D3 D4 E F1 F2 F3 F4 G NEG).each do |genotype|
  Genotype.find_or_create_by(code: genotype)
end

%w(A B C D E F G N).each do |g|
  Group.find_or_create_by(code: g)
end

%w(L180M A181T).each do |m|
  Mutation.find_or_create_by(code: m)
end

Institution.find_or_create_by(name: "Hospital Israelita Albert Einstein", abbreviation: "HIAE")

