class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :code, null: false

      t.timestamps
    end
  end
end
