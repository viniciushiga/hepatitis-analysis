class CreateMutations < ActiveRecord::Migration
  def change
    create_table :mutations do |t|
      t.string :code, null: false

      t.timestamps
    end
  end
end
