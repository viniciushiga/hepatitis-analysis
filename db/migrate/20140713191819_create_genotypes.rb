class CreateGenotypes < ActiveRecord::Migration
  def change
    create_table :genotypes do |t|
      t.string :code, null: false

      t.timestamps
    end
  end
end
