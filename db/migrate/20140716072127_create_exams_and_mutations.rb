class CreateExamsAndMutations < ActiveRecord::Migration
  def change
    create_table :exams_mutations, id: false do |t|
      t.belongs_to :exam,     index: true, null: false
      t.belongs_to :mutation, index: true, null: false
    end
  end
end
