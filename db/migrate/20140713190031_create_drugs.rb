class CreateDrugs < ActiveRecord::Migration
  def change
    create_table :drugs do |t|
      t.string :name, null: false

      t.timestamps
    end
  end
end
