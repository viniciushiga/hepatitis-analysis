class CreateExamsAndDrugs < ActiveRecord::Migration
  def change
    create_table :drugs_exams, id: false do |t|
      t.belongs_to :exam, index: false, null: false
      t.belongs_to :drug, index: false, null: false
    end
  end
end
