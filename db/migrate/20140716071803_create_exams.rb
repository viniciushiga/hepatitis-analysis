class CreateExams < ActiveRecord::Migration
  def change
    create_table :exams do |t|
      t.references :patient,     index: true, null: false
      t.date :examination_date,  null: false
      t.references :institution, index: true, null: false
      t.references :genotype,    index: true, null: false
      t.references :group,       index: true, null: false
      t.references :doctor,      index: true, null: false

      t.timestamps
    end
  end
end
