class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.string :name, null: false
      t.string :crm
      t.string :email
      t.string :phone

      t.timestamps
    end
  end
end
