class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string     :name,   null: false
      t.string     :gender, null: false, limit: 1
      t.date       :birthday
      t.string     :medical_record
      t.references :institution, index: true

      t.timestamps
    end
  end
end
