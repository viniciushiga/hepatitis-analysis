# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140716072133) do

  create_table "doctors", force: true do |t|
    t.string   "name",       null: false
    t.string   "crm"
    t.string   "email"
    t.string   "phone"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "drugs", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "drugs_exams", id: false, force: true do |t|
    t.integer "exam_id", null: false
    t.integer "drug_id", null: false
  end

  create_table "exams", force: true do |t|
    t.integer  "patient_id",       null: false
    t.date     "examination_date", null: false
    t.integer  "institution_id",   null: false
    t.integer  "genotype_id",      null: false
    t.integer  "group_id",         null: false
    t.integer  "doctor_id",        null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "exams", ["doctor_id"], name: "index_exams_on_doctor_id"
  add_index "exams", ["genotype_id"], name: "index_exams_on_genotype_id"
  add_index "exams", ["group_id"], name: "index_exams_on_group_id"
  add_index "exams", ["institution_id"], name: "index_exams_on_institution_id"
  add_index "exams", ["patient_id"], name: "index_exams_on_patient_id"

  create_table "exams_mutations", id: false, force: true do |t|
    t.integer "exam_id",     null: false
    t.integer "mutation_id", null: false
  end

  add_index "exams_mutations", ["exam_id"], name: "index_exams_mutations_on_exam_id"
  add_index "exams_mutations", ["mutation_id"], name: "index_exams_mutations_on_mutation_id"

  create_table "genotypes", force: true do |t|
    t.string   "code",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "groups", force: true do |t|
    t.string   "code",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "institutions", force: true do |t|
    t.string   "name",         null: false
    t.string   "abbreviation"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "mutations", force: true do |t|
    t.string   "code",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "patients", force: true do |t|
    t.string   "name",                     null: false
    t.string   "gender",         limit: 1, null: false
    t.date     "birthday"
    t.string   "medical_record"
    t.integer  "institution_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "patients", ["institution_id"], name: "index_patients_on_institution_id"

end
