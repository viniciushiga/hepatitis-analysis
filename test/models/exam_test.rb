# == Schema Information
#
# Table name: exams
#
#  id               :integer          not null, primary key
#  patient_id       :integer          not null
#  examination_date :date             not null
#  institution_id   :integer          not null
#  genotype_id      :integer          not null
#  group_id         :integer          not null
#  doctor_id        :integer          not null
#  created_at       :datetime
#  updated_at       :datetime
#
# Indexes
#
#  index_exams_on_doctor_id       (doctor_id)
#  index_exams_on_genotype_id     (genotype_id)
#  index_exams_on_group_id        (group_id)
#  index_exams_on_institution_id  (institution_id)
#  index_exams_on_patient_id      (patient_id)
#

require 'test_helper'

class ExamTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
end
