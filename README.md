# README #

### Requirements ###

* ruby 2.0.0+

### How do I get set up? ###

1. gem install bundler
2. bundle install
3. bundle exec rake db:migrate
4. bundle exec rails s
5. [Enjoy!](http://localhost:3000)
